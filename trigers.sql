use cinema;

delimiter //
CREATE TRIGGER update_count_log AFTER INSERT ON event
FOR EACH ROW 
BEGIN
insert into log_count(countType, count, eventId) values (1, 0, new.id);
insert into log_count(countType, count, eventId) values (2, 0, new.id);
insert into log_count(countType, count, eventId) values (3, 0, new.id);
END; //
delimiter ;

delimiter //
CREATE TRIGGER user_after_insert AFTER INSERT ON user
FOR EACH ROW
BEGIN
INSERT INTO log_user_discount (userId, discountCount) values (new.id, 0);
END; //
delimiter ;