package ua.epam.spring.hometask.dao.daoimp;


import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ua.epam.spring.hometask.config.ApplicationContextConfiguration;
import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.domain.Event;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationContextConfiguration.class)
public class EventDAOTest {

	@Autowired
	private EventDAO eventDAO;
	private Logger logger = Logger.getLogger(this.getClass().getName());

	@Test
	public void getAll() {
		List<Event> events = eventDAO.getAll();
		for(Event x : events){
			logger.info(x);
		}
		assertNotNull(events);
	}

	@Test
	public void getEventByName() {
		String name = "Joker";
		assertNotNull(eventDAO.getByName("Joker"));
	}
}