package ua.epam.spring.hometask.dao.daoimp;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ua.epam.spring.hometask.config.ApplicationContextConfiguration;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationContextConfiguration.class)
public class TicketDAOTest {

	@Autowired
	private TicketDAO ticketDAO;

	@Autowired
	private EventDAO eventDAO;

	private Logger logger = Logger.getLogger(this.getClass().getName());

	@Test
	public void getAll() {
		List<Ticket> tickets = ticketDAO.getAll();
		for (Ticket x : tickets){
			logger.info(x);
		}
		assertNotNull(tickets);
	}

	@Test
	public void getById() {
		Ticket ticket = ticketDAO.getById(Long.valueOf(1));
		assertNotNull(ticket);
	}

	@Test
	public void  GET_PURCHASED_TICKETS_FOR_EVENT(){
		Event event = eventDAO.getById(Long.valueOf(1));
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		List<Ticket> tickets = new ArrayList<>(ticketDAO.getPurchasedTicketsForEvent(event,  LocalDateTime.parse("2019-10-02 11:00", formatter)));
		for(Ticket x: tickets){
			logger.info(x);
		}
	}
}