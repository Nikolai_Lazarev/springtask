package ua.epam.spring.hometask.dao.daoimp;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ua.epam.spring.hometask.HomeContext;
import ua.epam.spring.hometask.config.ApplicationContextConfiguration;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.serviceimp.Roles;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationContextConfiguration.class)
public class UserDAOTest {

	@Autowired
	private UserDAO userDAO;
	private Logger logger = Logger.getLogger(this.getClass().getName());

	@Test
	public void getAll() {
		List<User> result = userDAO.getAll();
		for (User x : result) {
			logger.info(x);
		}
	}

	@Test
	@DisplayName("Assert if user was created")
	public void createUser() {
		User user = (User) HomeContext.getApplicationContext().getBean("user");
		user.setLastName("lazarev");
		user.setFirstName("nikolai");
		user.setEmail("user");
		user.setPassword("1234");
		user.setRoles(Roles.USER);
		LocalDate date = LocalDate.parse("2000-11-23");
		user.setBirthday(date);
		userDAO.create(user);
	}
}