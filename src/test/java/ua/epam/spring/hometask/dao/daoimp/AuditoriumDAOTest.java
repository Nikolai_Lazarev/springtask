package ua.epam.spring.hometask.dao.daoimp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ua.epam.spring.hometask.config.ApplicationContextConfiguration;
import ua.epam.spring.hometask.domain.Auditorium;

import java.util.Set;

import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationContextConfiguration.class)
public class AuditoriumDAOTest {

	@Autowired
	private AuditoriumDAO auditoriumDAO;
	private static final String AUDITORIUM_NAME = "firstAuditorium";
	private Logger logger = LogManager.getLogger(this.getClass().getName());


	@Test
	public void GET_ALL_AUDITORIUM() {
		Set<Auditorium> auditoriums = auditoriumDAO.getAll();
		for (Auditorium x : auditoriums) {
			logger.info(x);
		}
		assertNotNull(auditoriums);
	}

	@Test
	public void GET_AUDITORIUM_BY_NAME() {
		Auditorium auditorium = auditoriumDAO.getByName(AUDITORIUM_NAME);
		logger.info(auditorium);
		assertNotNull(auditorium);
	}
}