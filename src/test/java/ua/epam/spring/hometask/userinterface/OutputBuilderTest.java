package ua.epam.spring.hometask.userinterface;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ua.epam.spring.hometask.manager.LocalizationManager;


@RunWith(SpringRunner.class)
@ContextConfiguration(locations = "classpath:applicationContext.xml")
public class OutputBuilderTest {

    @Autowired
    private OutputBuilder outputBuilder;
    @Autowired
    private LocalizationManager localizationManager;
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Test
    public void TEST_OUTPUT() {
        String result;
        result = outputBuilder
                .asTitle("Ticket")
                .asRow("time", "10:00")
                .asRow("event", "Joker")
                .asRow(localizationManager.getValue("user.firstname"), "jack")
                .build();
        for(int i = 0; i<2; i++)
        logger.info(result);
    }
}