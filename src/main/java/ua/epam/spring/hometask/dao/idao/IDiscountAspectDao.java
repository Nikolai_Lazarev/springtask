package ua.epam.spring.hometask.dao.idao;

import ua.epam.spring.hometask.domain.User;

public interface IDiscountAspectDao {

	void logDiscount(User user, int discountAmount);

}
