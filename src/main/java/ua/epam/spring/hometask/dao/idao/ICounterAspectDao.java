package ua.epam.spring.hometask.dao.idao;

import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;

import java.util.Collection;

public interface ICounterAspectDao {
	Object logFindEventByName(Event event);
	Object logBookTicketForEvent(Collection<Ticket> tickets);
}
