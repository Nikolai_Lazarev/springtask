package ua.epam.spring.hometask.dao.idao;

import ua.epam.spring.hometask.domain.User;

import java.util.List;

public interface IUserDAO extends IBaseDAO<User> {
	List<User> getAll();
}
