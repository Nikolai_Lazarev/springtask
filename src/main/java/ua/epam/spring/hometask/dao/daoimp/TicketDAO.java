package ua.epam.spring.hometask.dao.daoimp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.HomeContext;
import ua.epam.spring.hometask.dao.idao.ITicketDAO;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class TicketDAO implements ITicketDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private EventDAO eventDAO;

	private RowMapper<Ticket> ticketRowMapper;

	@PostConstruct
	public void init() {
		ticketRowMapper = (resultSet, row) -> {
			Ticket ticket = (Ticket) HomeContext.getApplicationContext().getBean("ticket");
			ticket.setId(Long.valueOf(resultSet.getInt("id")));
			ticket.setDateTime(resultSet.getTimestamp("eventDate").toLocalDateTime());
			ticket.setUser(userDAO.getById(Long.valueOf(resultSet.getInt("userId"))));
			ticket.setEvent(eventDAO.getById(Long.valueOf(resultSet.getInt("eventId"))));
			ticket.setSeat(resultSet.getInt("seat"));
			return ticket;
		};
	}

	@Override
	public List<Ticket> getAll() throws EmptyResultDataAccessException {
		String sql = "SELECT * FROM ticket";
		return jdbcTemplate.query(sql, ticketRowMapper);
	}

	@Override
	public Ticket create(Ticket obj) throws EmptyResultDataAccessException {
		String query = "INSERT INTO ticket (userId, eventId, eventDate, seat) VALUES (?, ?, ? ,?)";
		jdbcTemplate.update(query, new Object[]{obj.getUser().getId(), obj.getEvent().getId(), obj.getDateTime(), obj.getSeat()});
		return jdbcTemplate.queryForObject("SELECT * FROM ticket WHERE userId = ? AND eventDate = ? AND seat = ?", new Object[]{obj.getUser().getId(), obj.getDateTime(), obj.getSeat()}, ticketRowMapper);
	}

	@Override
	public Ticket remove(Ticket obj) {
		return null;
	}

	@Override
	public Ticket getById(Long id) throws EmptyResultDataAccessException{
		String query = "SELECT * FROM ticket WHERE id = ?";
		return jdbcTemplate.queryForObject(query, new Object[]{id}, ticketRowMapper);

	}

	public Set<Ticket> getPurchasedTicketsForEvent(Event event, LocalDateTime localDateTime) throws EmptyResultDataAccessException{
		String sql = "SELECT * FROM ticket WHERE eventId = ? AND eventDate = ?";
		return new HashSet<>(jdbcTemplate.query(sql, new Object[]{event.getId(), localDateTime}, ticketRowMapper));
	}

}
