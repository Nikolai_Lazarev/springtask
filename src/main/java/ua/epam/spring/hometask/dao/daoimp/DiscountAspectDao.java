package ua.epam.spring.hometask.dao.daoimp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.dao.idao.IDiscountAspectDao;
import ua.epam.spring.hometask.domain.User;


@Component
public class DiscountAspectDao implements IDiscountAspectDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public void logDiscount(User user, int discountAmount) {
		while (!checkExistsRecordAboutUser(user)) {
			insertUserInDiscountAmount(user);
		}
		String query = "UPDATE logUserDiscount SET discountCount = discountCount + ? WHERE userId = ?";
		jdbcTemplate.update(query, new Object[]{discountAmount, user.getId()});
	}

	private void insertUserInDiscountAmount(User user) {
		String queryInsert = "INSERT INTO log_user_discount (userId, discountCount) VALUES (?, 0)";
		jdbcTemplate.update(queryInsert, new Object[]{user.getId()});
	}

	private boolean checkExistsRecordAboutUser(User user) {
		String querySelect = "SELECT count(*) AS \'count\' FROM log_user_discount WHERE userId = ?";
		Integer i = jdbcTemplate.queryForObject(querySelect, new Object[]{user.getId()}, (resultSet, j) -> resultSet.getInt("count"));
		if (i > 0) {
			return true;
		} else {
			return false;
		}
	}
}
