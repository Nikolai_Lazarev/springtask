package ua.epam.spring.hometask.dao.idao;

import ua.epam.spring.hometask.domain.DomainObject;

import java.util.List;

public interface IBaseDAO<T extends DomainObject> {
	T create(T obj);

	T remove(T obj);

	T getById(Long id);

	List<T> getAll();
}
