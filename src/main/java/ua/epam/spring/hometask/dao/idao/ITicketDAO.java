package ua.epam.spring.hometask.dao.idao;

import ua.epam.spring.hometask.domain.Ticket;

import java.util.List;

public interface ITicketDAO extends IBaseDAO<Ticket> {
	List<Ticket> getAll();
}
