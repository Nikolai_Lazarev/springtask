package ua.epam.spring.hometask.dao.idao;

import ua.epam.spring.hometask.domain.Event;

import java.util.List;

public interface IEventDAO extends IBaseDAO<Event> {
	List<Event> getAll();
}
