package ua.epam.spring.hometask.dao.daoimp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.HomeContext;
import ua.epam.spring.hometask.dao.idao.IEventDAO;
import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.EventRating;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.TreeMap;

@Component
public class EventDAO implements IEventDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	private RowMapper<Event> eventMapper;
	@Autowired
	private AuditoriumDAO auditoriumDAO;
	private RowMapper<TreeMap<LocalDateTime, Auditorium>> auditoriumsMapper;


	@PostConstruct
	private void init() {
		auditoriumsMapper = (res, row) -> {
			if(res == null){
				return null;
			}
			TreeMap<LocalDateTime, Auditorium> session = new TreeMap<>();
			session.put(res.getTimestamp("eventDate").toLocalDateTime(), auditoriumDAO.getByName(res.getString("auditoriumName")));
			return session;
		};
		eventMapper = (res, row) -> {
			Event event = (Event) HomeContext.getApplicationContext().getBean("event");
			event.setId(Long.valueOf(res.getInt("id")));
			event.setName(res.getString("eventName"));
			event.setBasePrice(res.getDouble("basePrice"));
			event.setRating(EventRating.valueOf(res.getString("ratingName").toUpperCase()));
			event.setAuditoriums(getAuditoriumForEvent(event.getName()));
			return event;
		};
	}

	@Override
	public List<Event> getAll() {
		String sql = "SELECT event.id, event.eventName, event.basePrice, eventrating.ratingName FROM Event LEFT JOIN eventrating ON eventrating.id = event.rating;";
		return jdbcTemplate.query(sql, eventMapper);
	}

	public Event getByName(String name) throws EmptyResultDataAccessException{
		String query = "SELECT event.id, event.eventName, event.basePrice, eventrating.ratingName FROM Event LEFT JOIN eventrating ON eventrating.id = event.rating WHERE event.eventName LIKE ?";
		return jdbcTemplate.queryForObject(query, new Object[]{name}, eventMapper);
	}

	public TreeMap<LocalDateTime, Auditorium> getAuditoriumForEvent(String eventName) throws EmptyResultDataAccessException{
		try {
			String sql = "SELECT eventDate, auditoriumName FROM session WHERE eventName = ?";
			return jdbcTemplate.queryForObject(sql, new Object[]{eventName}, auditoriumsMapper);
		}catch (EmptyResultDataAccessException e){
			return null;
		}
	}

	@Override
	public Event create(Event obj) throws EmptyResultDataAccessException {
		String putQuery = "INSERT INTO event (eventName, basePrice, rating) VALUES (?, ?, ?)";
		jdbcTemplate.update(putQuery, new Object[]{obj.getName(), obj.getBasePrice(), obj.getRating().ordinal ()+1});
		return getByName(obj.getName());
	}

	@Override
	public Event remove(Event obj) {
		return null;
	}

	@Override
	public Event getById(Long id) throws EmptyResultDataAccessException {
		String query = "SELECT event.id, event.eventName, event.basePrice, eventrating.ratingName FROM Event LEFT JOIN eventrating ON eventrating.id = event.rating WHERE event.id = ?";
		return jdbcTemplate.queryForObject(query, new Object[]{id}, eventMapper);
	}
}
