package ua.epam.spring.hometask.dao.daoimp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.HomeContext;
import ua.epam.spring.hometask.dao.idao.IUserDAO;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.serviceimp.Roles;

import javax.annotation.PostConstruct;
import java.util.List;


@Component
public class UserDAO implements IUserDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private RowMapper<User> userRowMapper;
	private static Logger logger = Logger.getLogger(UserDAO.class);

	@PostConstruct
	public void init() {
		userRowMapper = (resultSet, row) -> {
			User user = (User) HomeContext.getApplicationContext().getBean("user");
			user.setId(resultSet.getLong("id"));
			user.setEmail(resultSet.getString("email"));
			user.setFirstName(resultSet.getString("firstName"));
			user.setLastName(resultSet.getString("lastName"));
			user.setRoles(Roles.valueOf(resultSet.getString("role").toUpperCase()));
			user.setPassword(resultSet.getString("password"));
			user.setBirthday(resultSet.getDate("birthday").toLocalDate());
			return user;
		};
	}

	@Override
	public List<User> getAll() throws EmptyResultDataAccessException{
		List<User> result;
		try {
			String sql = "SELECT * FROM user";
			result = jdbcTemplate.query(sql, userRowMapper);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
		return result;
	}

	@Override
	public User create(User obj) throws EmptyResultDataAccessException{
		try {
			String query = "INSERT INTO user (firstName, lastName, email, role, birthday, password) VALUES (?, ?, ?, ?, ?, md5(?))";
			jdbcTemplate.update(query, new Object[]{obj.getFirstName(), obj.getLastName(), obj.getEmail(), obj.getRoles().toString(), obj.getBirthday(), obj.getPassword()});
			return getUserByEmailAndPassword(obj.getEmail(), obj.getPassword());
		} catch (DuplicateKeyException e) {
			logger.error(e);
			logger.info("That user already exist");
			return null;
		}
	}

	@Override
	public User remove(User obj) {
		return null;
	}

	@Override
	public User getById(Long id) {
		return null;
	}

	public User getUserByEmailAndPassword(String email, String password) throws EmptyResultDataAccessException{
		try {
			String sql = "SELECT * FROM user WHERE email = ? AND password = md5(?)";
			User user = jdbcTemplate.queryForObject(sql, new Object[]{email, password}, userRowMapper);
			return user;
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}
}
