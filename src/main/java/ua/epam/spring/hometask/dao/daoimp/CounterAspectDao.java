package ua.epam.spring.hometask.dao.daoimp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.dao.idao.ICounterAspectDao;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class CounterAspectDao implements ICounterAspectDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private EventDAO eventDAO;

	@Override
	public Event logFindEventByName(Event event) throws EmptyResultDataAccessException {
		String query = "UPDATE log_count SET count = count + 1 WHERE eventId = ? AND countType = 1";
		while (!checkExistsRecordForEvent(event)) {
			addEventToLog(event);
		}
		jdbcTemplate.update(query, new Object[]{event.getId()});
		return event;
	}

	@Override
	public Collection<Ticket> logBookTicketForEvent(Collection<Ticket> tickets) throws EmptyResultDataAccessException {
		List<Ticket> listTickets = new ArrayList<>(tickets);
		Event event = eventDAO.getById(listTickets.get(0).getEvent().getId());
		Integer count = tickets.size();
		String query = "UPDATE log_count SET count = count + ? WHERE eventId = ? AND countType = 2";
		while (!checkExistsRecordForEvent(event)){
			addEventToLog(event);
		}
		jdbcTemplate.update(query, new Object[]{count, event.getId()});
		return tickets;
	}

	private boolean checkExistsRecordForEvent(Event event) {
		String query = "SELECT count(*) AS \'count\' FROM log_count WHERE eventId = ?";
		Integer count = jdbcTemplate.queryForObject(query, new Object[]{event.getId()}, (resultSet, i) -> resultSet.getInt("count"));
		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}

	private void addEventToLog(Event event) {
		String queryEventByName = "INSERT INTO log_count (countType, count, eventId) values (1, 0, ?)";
		String queryBookingTicket = "INSERT INTO log_count (countType, count, eventId) values (2, 0, ?)";
		String queryEventPrice = "INSERT INTO log_count (countType, count, eventId) values (3, 0, ?)";
		jdbcTemplate.update(queryEventByName, new Object[]{event.getId()});
		jdbcTemplate.update(queryBookingTicket, new Object[]{event.getId()});
		jdbcTemplate.update(queryEventPrice, new Object[]{event.getId()});
	}
}
