package ua.epam.spring.hometask.dao.idao;

import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.domain.Auditorium;

import java.util.Set;

@Component
public interface IAuditoriumDAO {
	Set<Auditorium> getAll();
	Auditorium getByName(String name);
}

