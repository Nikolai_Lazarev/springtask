package ua.epam.spring.hometask.dao.daoimp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.HomeContext;
import ua.epam.spring.hometask.dao.idao.IAuditoriumDAO;
import ua.epam.spring.hometask.domain.Auditorium;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.HashSet;
import java.util.Set;


@Component
public class AuditoriumDAO implements IAuditoriumDAO {

	@Autowired
	JdbcTemplate jdbcTemplate;
	RowMapper<Auditorium> mapper;

	@PostConstruct
	public void init() {
		mapper = ((resultSet, i) -> {
			Auditorium auditorium = HomeContext.getApplicationContext().getBean("auditorium", Auditorium.class);
			auditorium.setNumberOfSeats(resultSet.getInt("numberOfSeats"));
			auditorium.setName(resultSet.getString("name"));
			return auditorium;
		});
	}

	@Override
	public Set<Auditorium> getAll() throws EmptyResultDataAccessException {
		String sql = "SELECT auditorium.name, auditorium.numberOfSeats FROM auditorium";
		Set<Auditorium> result = new HashSet<>(jdbcTemplate.query(sql, mapper));
		result.forEach(a -> {
			a.setVipSeats(getVipSeatsByAuditoriumName(a.getName()));
		});
		return result;
	}

	@Override
	public Auditorium getByName(String name) throws EmptyResultDataAccessException {
		String sql = "SELECT auditorium.name, auditorium.numberOfSeats FROM auditorium WHERE auditorium.name = ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{name}, mapper);
	}

	private Set<Long> getVipSeatsByAuditoriumName(String auditoriumName) throws EmptyResultDataAccessException {
		String sql = "SELECT numberOfSeat FROM vipseats WHERE auditoriumName = ?";
		return new HashSet<>(jdbcTemplate.query(sql, new Object[]{auditoriumName}, (res, row) -> res.getLong("numberOfSeat")));
	}
}
