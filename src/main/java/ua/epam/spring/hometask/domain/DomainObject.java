package ua.epam.spring.hometask.domain;

import org.springframework.beans.factory.annotation.Autowired;
import ua.epam.spring.hometask.userinterface.OutputBuilder;

/**
 * @author Yuriy_Tkach
 */
public abstract class DomainObject {

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Autowired
	protected OutputBuilder outputBuilder;
	
}
