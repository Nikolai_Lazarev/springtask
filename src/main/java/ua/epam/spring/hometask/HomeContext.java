package ua.epam.spring.hometask;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.config.ApplicationContextConfiguration;
import ua.epam.spring.hometask.domain.User;


@Component
public class HomeContext{
	private static ApplicationContext instance;
	private User user;

	public HomeContext(){}

	public static ApplicationContext getApplicationContext() {
		if(instance==null){
			instance = new AnnotationConfigApplicationContext(ApplicationContextConfiguration.class);
		}
		return instance;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}
}
