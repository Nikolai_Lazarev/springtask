package ua.epam.spring.hometask.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
@ComponentScan("ua.epam.spring.hometask")
public class ApplicationContextConfiguration {

	@Bean
	public JdbcTemplate getJdbcTemplate() {
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
		driverManagerDataSource.setDriverClassName(com.mysql.jdbc.Driver.class.getName());
		driverManagerDataSource.setUrl("jdbc:mysql://localhost:3306/cinema?serverTimezone=Europe/Moscow&amp");
		driverManagerDataSource.setUsername("root");
		driverManagerDataSource.setPassword("123456");
		JdbcTemplate jdbcTemplate = new JdbcTemplate(driverManagerDataSource);
		return jdbcTemplate;
	}
}
