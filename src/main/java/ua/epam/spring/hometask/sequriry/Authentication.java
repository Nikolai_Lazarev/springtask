package ua.epam.spring.hometask.sequriry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.HomeContext;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.manager.Manager;
import ua.epam.spring.hometask.service.UserService;
import ua.epam.spring.hometask.serviceimp.Roles;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.TreeSet;

@Component
public class Authentication {

	private Manager localizationManager;
	private UserService userService;

	public Authentication() {
	}

	@Autowired
	public Authentication(Manager localizationManager, UserService userService) {
		this.localizationManager = localizationManager;
		this.userService = userService;
	}

	public User registration(String firstName, String lastName, String email, String birthday, String password) {
		User user = (User)HomeContext.getApplicationContext().getBean("user");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setPassword(password);
		user.setRoles(Roles.USER);
		user.setEmail(email);
		user.setBirthday(LocalDate.parse(birthday));
		user.setTickets(new TreeSet<>());
		return userService.save(user);
	}

	public User authorization(String email, String password) {
		return userService.getUserByEmailAndPassword(email, password);
	}
}
