package ua.epam.spring.hometask.aspects;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ExceptionAspect {

	private static Logger logger = Logger.getLogger(CounterAspect.class);


	@Pointcut("execution (* ua.epam.spring.hometask.dao.daoimp.*.*(..))")
	private void afterEmptyResultDataAccessException(){}

	@Around("afterEmptyResultDataAccessException()")
	public Object logException(ProceedingJoinPoint proceedingJoinPoint){
		Object statement = null;
		try{
			statement = proceedingJoinPoint.proceed();
		}catch (Throwable e){
			logger.error(e);
			statement = null;
		}
		return statement;
	}


}
