package ua.epam.spring.hometask.aspects;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.dao.idao.IDiscountAspectDao;
import ua.epam.spring.hometask.domain.User;

import java.util.Arrays;

@Aspect
@Component
public class DiscountAspect {

	@Autowired
	private IDiscountAspectDao discountAspectDao;

	@Pointcut("execution (* ua.epam.spring.hometask.serviceimp.DiscountServiceImp.getDiscount(..))")
	private void logDiscountPointcut() {
	}

	@AfterReturning(value = "logDiscountPointcut()", returning = "returnStatement")
	private void logDiscount(JoinPoint joinPoint, Object returnStatement) {
		User user;
		Object args[] = joinPoint.getArgs();
		user = (User) Arrays.stream(args).filter(obj -> obj instanceof User).findAny().get();
		byte discount = (byte) returnStatement;
		if (discount > 0) {
			discountAspectDao.logDiscount(user, 1);
		}
	}
}
