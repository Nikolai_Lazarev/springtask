package ua.epam.spring.hometask.aspects;


import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.dao.idao.ICounterAspectDao;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Aspect
@Component
public class CounterAspect {

	private static Logger logger = Logger.getLogger(CounterAspect.class);

	@Autowired
	private ICounterAspectDao counterAspectDao;

	@Pointcut("execution (* ua.epam.spring.hometask.serviceimp.BookingServiceImp.bookTickets(..))")
	private void logBookingTicket() {
	}

	@Pointcut("execution (* ua.epam.spring.hometask.serviceimp.EventServiceImp.getByName(..))")
	private void afterGetByName() {
	}

	@Before("logBookingTicket()")
	private void logBookingTicketToBase(JoinPoint joinPoint) {
		List<Ticket> tickets = new ArrayList<Ticket>((TreeSet<Ticket>) Arrays.stream(joinPoint.getArgs()).collect(Collectors.toList()).get(0));
		counterAspectDao.logBookTicketForEvent(tickets);
	}

	@AfterReturning(value = "afterGetByName()", returning = "returnValue")
	private void logEventByName(Object returnValue) {
		Event event = (Event) returnValue;
		if (event != null) {
			counterAspectDao.logFindEventByName(event);
		}
	}
}
