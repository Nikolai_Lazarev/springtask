package ua.epam.spring.hometask.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LuckyWinnerAspect {

	@Pointcut("execution (* ua.epam.spring.hometask.serviceimp.BookingServiceImp.getTicketsPrice(..))")
	private void luckyWinnerPointcut() {
	}


	@Around("luckyWinnerPointcut()")
	private double getLuckyWinnerPrice(ProceedingJoinPoint proceedingJoinPoint) {
		double statement = 0;
		try {
			statement = (double) proceedingJoinPoint.proceed();
			if(Math.random() > 0.5){
				statement = 0;
			}
		} catch (Throwable e) {}
		return statement;
	}
}
