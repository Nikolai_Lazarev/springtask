package ua.epam.spring.hometask;

import org.springframework.context.ApplicationContext;
import ua.epam.spring.hometask.userinterface.NotAuthorizedInterface;
import ua.epam.spring.hometask.userinterface.UserInterface;


public class Main {

	private static UserInterface userInterface;

	public static void main(String[] args) {
		ApplicationContext context = HomeContext.getApplicationContext();
		userInterface = context.getBean("notAuthorizedInterface", NotAuthorizedInterface.class);
		userInterface.show();
	}
}
