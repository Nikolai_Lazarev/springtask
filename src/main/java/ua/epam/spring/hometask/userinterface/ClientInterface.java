package ua.epam.spring.hometask.userinterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.manager.ConfigManager;
import ua.epam.spring.hometask.manager.LocalizationManager;
import ua.epam.spring.hometask.service.BookingService;
import ua.epam.spring.hometask.service.DiscountService;
import ua.epam.spring.hometask.service.EventService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


@Component("clientInterface")
public class ClientInterface extends UserInterface {

	@Autowired
	private EventService eventService;
	@Autowired
	private BookingService bookingService;
	@Autowired
	private DiscountService discountService;

	private User user;

	public ClientInterface() {
		super();
		this.user = homeContext.getUser();
	}

	@Autowired
	public ClientInterface(ConfigManager manager, LocalizationManager localizationManager) {
		super(manager, localizationManager);
	}


	@Override
	protected void doCommands() {
		user = homeContext.getUser();
		String input;
		while (!(input = scanner.nextLine()).equalsIgnoreCase(manager.getValue("command.exit"))) {
			switch (input) {
				case "1":
					showEvents();
					break;
				case "2":
					choiceEvent();
					break;
				case "3":
					logout();
					break;
				default:
					searchDefault(input);
			}
			printCommandList(this);
		}
	}

	private void choiceEvent() {
		String input;
		String eventName;
		List<String> eventNames = new ArrayList<>();
		List<Event> events = new ArrayList<>(eventService.getAll());
		for (Event event : eventService.getAll()) {
			eventNames.add(event.getName());
		}
		Mapper<String> mapper = new Mapper<>(eventNames);
		mapper.printMap();
		logger.info(localizationManager.getValue("ClientInterface.choice.event"));
		eventName = mapper.getFromMap(CheckUserInput.checkIntInput(mapper.getLowMapBorder(), mapper.getHighMapBorder()));
		Set<Ticket> tickets = user.getTickets();
		int auditoriumNumber;
		int ticketAmount;
		Set<Long> seats = new TreeSet<>();
		Event event = events.stream().filter(ev -> ev.getName().equals(eventName)).findAny().get();
		List<LocalDateTime> times = new ArrayList<>(event.getAuditoriums().keySet());
		Mapper<Auditorium> auditoriumMapper = new Mapper<>(event.getAuditoriums().values());
		auditoriumMapper.printMap();
		auditoriumNumber = CheckUserInput.checkIntInput(auditoriumMapper.getLowMapBorder(), auditoriumMapper.getHighMapBorder());
		List<Long> freeSeats = new ArrayList<>(bookingService.getFreeSeats(event, times.get(auditoriumNumber)));
		printSeats(freeSeats);
		logger.info("Input tickets amount");
		ticketAmount = CheckUserInput.checkIntInput(manager.getValue("command.back"));
		for (int i = 0; i < ticketAmount; i++) {
			logger.info(localizationManager.getValue("ClientInterface.choice.seat"));
			Long seat = CheckUserInput.checkSeatInput(freeSeats);
			seats.add(seat);
			tickets.add(new Ticket(user, event, times.get(auditoriumNumber), seat));
		}
		double price = 0.0;
		price = bookingService.getTicketsPrice(event, times.get(auditoriumNumber), user, seats);
		logger.info(localizationManager.getValue("ClientInterface.buy.finally.price") + " " + price);
		logger.info(localizationManager.getValue("ClientInterface.buy.accept"));
		if (CheckUserInput.isAccept()) {
			bookingService.bookTickets(tickets);
			logger.info(localizationManager.getValue("ClientInterface.buy.accept.success"));
		} else {
			logger.info(localizationManager.getValue("ClientInterface.buy.accept.failed"));
		}

	}

	private void showEvents() {
		ArrayList<Event> arrayList = (ArrayList<Event>) eventService.getAll();
		arrayList.forEach(logger::info);
	}

	private void printSeats(List<Long> seats) {
		String seatSting = "\n";
		int freeSeatsSize = seats.size();
		for (int i = 0; i < freeSeatsSize; i++) {
			if (seats.get(i) >= 0) {
				if (i % 10 == 0) {
					seatSting += "\n";
				}
				seatSting += seats.get(i) + " ";
			} else {
				if (i % 10 == 0) {
					seatSting += "\n";
				}
				seatSting += "N" + " ";
			}
		}
		logger.info(seatSting);
	}

	private void searchDefault(String input) {
		logger.info(eventService.getByName(input));
	}

}
