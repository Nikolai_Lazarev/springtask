package ua.epam.spring.hometask.userinterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.HomeContext;
import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.EventRating;
import ua.epam.spring.hometask.manager.ConfigManager;
import ua.epam.spring.hometask.manager.LocalizationManager;
import ua.epam.spring.hometask.service.AuditoriumService;
import ua.epam.spring.hometask.service.BookingService;
import ua.epam.spring.hometask.service.EventService;
import ua.epam.spring.hometask.service.UserService;

import java.time.LocalDateTime;
import java.util.TreeMap;

@Component("administratorInterface")
public class AdministratorInterface extends UserInterface {

	@Autowired
	private EventService eventService;
	@Autowired
	private AuditoriumService auditoriumService;
	@Autowired
	private BookingService bookingService;


	public AdministratorInterface(ConfigManager manager, LocalizationManager localizationManager, UserService userService) {
		super(manager, localizationManager);
	}


	public AdministratorInterface() {
	}

	@Override
	protected void doCommands() {
		String input;
		while(!(input = scanner.nextLine()).equalsIgnoreCase(manager.getValue("command.exit"))) {
			switch (input) {
				case "1":
					createEvent();
					break;
				default:
			}
			printCommandList(this);
		}
	}

	private void createEvent() {
		Event event = (Event) HomeContext.getApplicationContext().getBean("event");
		logger.info(localizationManager.getValue("AdministratorInterface.create.event.name"));
		event.setName(scanner.nextLine());
		logger.info(localizationManager.getValue("AdministratorInterface.create.event.rating"));
		EventRating eventRating[] = EventRating.values();
		Mapper<EventRating> mapper = new Mapper<>();
		mapper.mapFrom(EventRating.values());
		mapper.printMap();
		event.setRating(mapper.getFromMap(CheckUserInput.checkIntInput(mapper.getLowMapBorder(), mapper.getHighMapBorder())));
		logger.info(localizationManager.getValue("AdministratorInterface.create.event.price"));
		event.setBasePrice(CheckUserInput.checkIntInput());
		logger.info(eventService.save(event));
	}

//	private void getPurchasedTickets() {
//		Mapper<Event> eventMapper = new Mapper<>();
//		eventMapper.mapFrom(eventService.getAll());
//		eventMapper.printMap();
//		eventMapper.choiceInMap();
//		bookingService.getPurchasedTicketsForEvent(eventMapper.choiceInMap());
//	}
}
