package ua.epam.spring.hometask.userinterface;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.epam.spring.hometask.HomeContext;
import ua.epam.spring.hometask.manager.ConfigManager;
import ua.epam.spring.hometask.manager.LocalizationManager;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.HashSet;
import java.util.Scanner;

public class CheckUserInput {

	private static Scanner scanner = new Scanner(System.in);
	private static ConfigManager manager = (ConfigManager) HomeContext.getApplicationContext().getBean("configManager");
	private static Logger logger = LogManager.getLogger(CheckUserInput.class);
	private static LocalizationManager localizationManager = (LocalizationManager) HomeContext.getApplicationContext().getBean("localizationManager");

	private static boolean checkInt(String input) {
		return input.matches("\\d+");
	}

	private static boolean checkInt(String input, int min, int max) {
		return input.matches("\\d+") && Integer.valueOf(input) >= min && Integer.valueOf(input) < max;
	}

	public static boolean isAccept() {
		String input;
		while (!((input = scanner.nextLine()).equalsIgnoreCase(manager.getValue("command.accept")) || input.equalsIgnoreCase(manager.getValue("command.cancel")))) {
			logger.info(localizationManager.getValue("user.input.failure"));
		}
		if (input.equalsIgnoreCase(manager.getValue("command.accept"))) {
			return true;
		}
		return false;
	}

	public static Integer checkIntInput(String exitCommand) {
		String input;
		while (!checkInt(input = scanner.nextLine()) || input.equalsIgnoreCase(exitCommand)) {
			logger.info(localizationManager.getValue("user.input.failure"));
		}
		return Integer.valueOf(input);
	}

	public static LocalDateTime inputDateTime() {
		String input;
		while (!(input = scanner.nextLine()).matches("\\d\\d\\d\\d-\\d\\d-\\d\\d \\d\\d:\\d\\d")) {
			logger.info(localizationManager.getValue("user.input.failure"));
		}
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		return LocalDateTime.parse(input, formatter);
	}

	public static Integer checkIntInput() {
		return checkIntInput(manager.getValue("command.back"));
	}

	public static Integer checkIntInput(int min, int max, String exitCommand) {
		String input;
		while (!checkInt(input = scanner.nextLine(), min, max) || input.equalsIgnoreCase(exitCommand)) {
			logger.info(localizationManager.getValue("user.input.failure"));
		}
		return Integer.valueOf(input);
	}

	public static Integer checkIntInput(int min, int max) {
		return checkIntInput(min, max, manager.getValue("command.back"));
	}

	private static boolean checkSeat(Collection<Long> freeSeat, Long seat) {
		if (new HashSet<>(freeSeat).contains(seat)) return true;
		return false;
	}

	public static Long checkSeatInput(Collection<Long> freeSeats) {
		Long seat;
		while (!checkSeat(freeSeats, (seat = Long.valueOf(CheckUserInput.checkIntInput(localizationManager.getValue("command.back")))))) {
			logger.info(localizationManager.getValue("user.input.failure"));
		}
		return seat;
	}

}
