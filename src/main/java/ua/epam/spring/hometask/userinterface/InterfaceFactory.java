package ua.epam.spring.hometask.userinterface;

import org.springframework.context.ApplicationContext;
import ua.epam.spring.hometask.HomeContext;
import ua.epam.spring.hometask.domain.User;

public class InterfaceFactory {

	private static ApplicationContext springContext = HomeContext.getApplicationContext();

	public static UserInterface getClientInterface(User user) {
		if (user == null) {
			return springContext.getBean("notAuthorizedInterface", NotAuthorizedInterface.class);
		}
		switch (user.getRoles()) {
			case ADMIN:
				return (AdministratorInterface)springContext.getBean("administratorInterface",user);
			case USER:
				return (ClientInterface)springContext.getBean("clientInterface", user);
			default:
		}
		return null;
	}
}
