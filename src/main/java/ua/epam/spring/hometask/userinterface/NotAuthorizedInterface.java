package ua.epam.spring.hometask.userinterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.HomeContext;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.manager.ConfigManager;
import ua.epam.spring.hometask.manager.LocalizationManager;
import ua.epam.spring.hometask.sequriry.Authentication;

import java.time.format.DateTimeFormatter;

@Component("notAuthorizedInterface")
public class NotAuthorizedInterface extends UserInterface {

	@Autowired
	private Authentication authentication;


	public NotAuthorizedInterface() {
	}

	@Autowired
	private HomeContext homeContext;

	@Autowired
	public NotAuthorizedInterface(ConfigManager manager, LocalizationManager localizationManager) {
		super(manager, localizationManager);
	}

	@Override
	protected void doCommands() {
		String input;
		while (!(input = scanner.nextLine()).equalsIgnoreCase(manager.getValue("command.exit"))) {
			switch (input) {
				case "1":
					User user = authorization();
					if (user != null) {
						homeContext.setUser(user);
						UserInterface userInterface = InterfaceFactory.getClientInterface(user);
						userInterface.show();
					} else {
						logger.info(localizationManager.getValue("NotAuthorizedInterface.error.not.found.email"));
					}
					break;
				case "2":
					user = registration();
					if (user != null) {
						homeContext.setUser(user);
						InterfaceFactory.getClientInterface(user).show();
					} else {
						logger.info(localizationManager.getValue("NotAuthorizedInterface.error.user.exist"));
					}
					break;
				default:
			}
			printCommandList(this);
		}
	}

	private User registration() {
		String userName;
		String userLastName;
		String userEmail;
		String userBirthday;
		logger.info(localizationManager.getValue("NotAuthorizedInterface.first.name"));
		userName = scanner.nextLine();
		logger.info(localizationManager.getValue("NotAuthorizedInterface.last.name"));
		userLastName = scanner.nextLine();
		logger.info(localizationManager.getValue("NotAuthorizedInterface.email"));
		userEmail = scanner.nextLine();
		logger.info(localizationManager.getValue("NotAuthorizedInterface.password"));
		String password = scanner.nextLine();
		logger.info(localizationManager.getValue("NotAuthorizedInterface.birthday"));
		while (!(userBirthday = scanner.nextLine()).matches("\\d\\d\\d\\d-\\d\\d-\\d\\d")) {
			logger.info(localizationManager.getValue("NotAuthorizedInterface.birthday"));
		}
		return authentication.registration(userName, userLastName, userEmail, userBirthday, password);
	}

	private User authorization() {
		logger.info(localizationManager.getValue("NotAuthorizedInterface.email"));
		String email = scanner.nextLine();
		logger.info(localizationManager.getValue("NotAuthorizedInterface.password"));
		String password = scanner.nextLine();
		return authentication.authorization(email, password);
	}
}
