package ua.epam.spring.hometask.userinterface;

import org.apache.log4j.Logger;

import java.util.*;

public class Mapper<T> {

	private Map<Integer, T> map;
	private Logger logger = Logger.getLogger(this.getClass().getName());
	private List<Integer> keys;
	private List<T> values;
	private int lowMapBorder;
	private int highMapBorder;

	public Mapper() {
		this.map = new TreeMap<>();
	}

	public Mapper(Collection<T> collection) {
		this.map = new TreeMap<>();
		mapFrom(collection);
	}

	public Mapper(T[] arr) {
		this.map = new TreeMap<>();
		mapFrom(arr);
	}

	public void mapFrom(Collection<T> list) {
		Iterator<T> iterator = list.iterator();
		int i = 0;
		for (T t : list) {
			map.put(i, t);
			i++;
		}
		keys = new ArrayList<>(map.keySet());
		values = new ArrayList<>(map.values());
		highMapBorder = keys.size();
		lowMapBorder = 0;
	}

	public void mapFrom(T[] arr) {
		for (int i = 0; i < arr.length; i++) {
			map.put(i, arr[i]);
		}
		keys = new ArrayList<>(map.keySet());
		values = new ArrayList<>(map.values());
		highMapBorder = keys.size();
		lowMapBorder = 0;
	}

	public void printMap() {
		for (int i = 0; i < keys.size(); i++) {
			logger.info(keys.get(i) + " - " + values.get(i));
		}
	}

	public T getFromMap(int fromIndex){
		return map.get(fromIndex);
	}

	public int getLowMapBorder() {
		return lowMapBorder;
	}

	public int getHighMapBorder() {
		return highMapBorder;
	}
}
