package ua.epam.spring.hometask.userinterface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.manager.ConfigManager;

import javax.annotation.PostConstruct;

@Component
@Scope("prototype")
public class OutputBuilder {

    private String output;
    @Autowired
    private ConfigManager configManager;

    private OutputBuilder() {
        output = "";
    }

    @PostConstruct
    private void init() {
        output = "\n";
    }

    public OutputBuilder asRow(String key, String value) {
        output += key
                + configManager.getValue("output.row.after.key")
                + configManager.getValue("output.tab")
                + value
                + configManager.getValue("output.end.line");
        return this;
    }

    public OutputBuilder asTitle(String key) {
        output += configManager.getValue("output.title.before.key")
                + key
                + configManager.getValue("output.title.after.key")
                + configManager.getValue("output.end.line");
        return this;
    }

    public String build() {
        return output += configManager.getValue("output.build")
                + configManager.getValue("output.end.line");
    }

    public OutputBuilder asRow(String key, Number value) {
        asRow(key, String.valueOf(value));
        return this;
    }

    public OutputBuilder asRow(String key, Object value) {
        asRow(key, String.valueOf(value));
        return this;
    }
}
