package ua.epam.spring.hometask.userinterface;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import ua.epam.spring.hometask.HomeContext;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.manager.ConfigManager;
import ua.epam.spring.hometask.manager.LocalizationManager;

import java.util.Scanner;

public abstract class UserInterface {

	@Autowired
	protected LocalizationManager localizationManager;
	@Autowired
	protected ConfigManager manager;
	protected User user;
	@Autowired
	protected HomeContext homeContext;
	protected Scanner scanner;
	protected Logger logger = Logger.getLogger(this.getClass().getName());

	public UserInterface(ConfigManager manager, LocalizationManager localizationManager) {
		this.manager = manager;
		this.localizationManager = localizationManager;
		this.scanner = new Scanner(System.in);
	}

	public UserInterface(User user) {
		this.user = user;
	}

	public UserInterface() {
		this.scanner = new Scanner(System.in);
	}

	protected void printCommandList(UserInterface userInterface)
	{
		String fullClassName = userInterface.getClass().getName();
		String className = fullClassName.substring(fullClassName.lastIndexOf(".")+1);
		logger.info(localizationManager.getValue(className+"."+ "commands"));
	}

	public void show(){
		printCommandList(this);
		this.doCommands();
	}

	protected void logout(){
		homeContext.setUser(null);
		InterfaceFactory.getClientInterface(homeContext.getUser()).show();
	}

	protected abstract void doCommands();
}
