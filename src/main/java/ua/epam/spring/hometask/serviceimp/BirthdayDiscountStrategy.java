package ua.epam.spring.hometask.serviceimp;

import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.DiscountStrategy;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Component
public class BirthdayDiscountStrategy implements DiscountStrategy {
	@Override
	public byte getDiscountStrategy(User user, Event event, LocalDateTime time) {
		LocalDateTime userBirthday = LocalDateTime.of(0000, user.getBirthday().getMonthValue(), user.getBirthday().getDayOfMonth(), 0, 0);
		LocalDateTime eventDate = LocalDateTime.of(0000, time.getMonthValue(), time.getDayOfMonth(), 0, 0);
		if (userBirthday.minus(5, ChronoUnit.DAYS).isBefore(eventDate) && userBirthday.plus(5, ChronoUnit.DAYS).isAfter(eventDate)) {
			return 10;
		}
		return 0;
	}
}
