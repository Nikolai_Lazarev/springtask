package ua.epam.spring.hometask.serviceimp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.dao.daoimp.AuditoriumDAO;
import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.service.AuditoriumService;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Set;

@Component
public class AuditoriumServiceImp implements AuditoriumService {

	@Autowired
	AuditoriumDAO auditoriumDAO;

	public AuditoriumServiceImp(AuditoriumDAO auditoriumDAO) {
		this.auditoriumDAO = auditoriumDAO;
	}

	@Nonnull
	@Override
	public Set<Auditorium> getAll() {
		Set<Auditorium> auditoriumSet = auditoriumDAO.getAll();
		return auditoriumSet;
	}

	@Nullable
	@Override
	public Auditorium getByName(@Nonnull String name) {
		return auditoriumDAO.getByName(name);
	}
}
