package ua.epam.spring.hometask.serviceimp;

import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.DiscountStrategy;

import java.time.LocalDateTime;

@Component
public class TenTicketsDiscountStrategy implements DiscountStrategy {
	@Override
	public byte getDiscountStrategy(User user, Event event, LocalDateTime time) {
		if(user.getTickets().size()%10==0 && user.getTickets().size()>0){
			return 20;
		}
		return 0;
	}
}
