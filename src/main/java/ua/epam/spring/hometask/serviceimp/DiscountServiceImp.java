package ua.epam.spring.hometask.serviceimp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.DiscountService;
import ua.epam.spring.hometask.service.DiscountStrategy;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.List;

@Component
public class DiscountServiceImp implements DiscountService {


	@Autowired
	List<DiscountStrategy> discountStrategies;

	public DiscountServiceImp() {
	}

	public DiscountServiceImp(List<DiscountStrategy> discountStrategies) {
		this.discountStrategies = discountStrategies;
	}

	@Override
	public byte getDiscount(@Nullable User user, @Nonnull Event event, @Nonnull LocalDateTime airDateTime, long numberOfTickets) {
		byte maxDiscount = 0;
		for (DiscountStrategy x : discountStrategies) {
			byte discount = x.getDiscountStrategy(user, event, airDateTime);
			if (discount > maxDiscount) maxDiscount = discount;
		}
		return maxDiscount;
	}
}

