package ua.epam.spring.hometask.serviceimp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.dao.daoimp.EventDAO;
import ua.epam.spring.hometask.dao.daoimp.TicketDAO;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.EventRating;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.BookingService;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Component
public class BookingServiceImp implements BookingService {

	@Autowired
	private TicketDAO ticketDAO;
	@Autowired
	private EventDAO eventDAO;
	@Autowired
	private DiscountServiceImp discountServiceImp;


	public BookingServiceImp() {
	}

	@Autowired
	public BookingServiceImp(TicketDAO ticketDAO, EventDAO eventDAO, DiscountServiceImp discountServiceImp) {
		this.ticketDAO = ticketDAO;
		this.eventDAO = eventDAO;
		this.discountServiceImp = discountServiceImp;
	}

	@Override
	public double getTicketsPrice(@Nonnull Event event, @Nonnull LocalDateTime dateTime, @Nullable User user, @Nonnull Set<Long> seats) {
		double finalPrice = 0;
		for (Long x : seats) {
			double price = event.getBasePrice();
			Set<Long> eventSeats = event.getAuditoriums().get(dateTime).getVipSeats();
			for (Long evSeat : eventSeats) {
				if (seats.contains(evSeat)) {
					price *= 2;
				}
			}
			if (event.getRating() == EventRating.HIGH) {
				price *= 1.2;
			}
			price *= (100.0 - discountServiceImp.getDiscount(user, event, dateTime, 1)) / 100.0;
			finalPrice += price;
		}
		return finalPrice;
	}

	@Override
	public void bookTickets(@Nonnull Set<Ticket> tickets) {
		tickets.forEach(Ticket -> {
			ticketDAO.create(Ticket);
		});
	}

	@Override
	public Collection<Long> getFreeSeats(Event event, LocalDateTime dateTime) {
		Set<Long> bookSeats = getPurchasedTicketsForEvent(event, dateTime)
				.stream()
				.mapToLong(Ticket::getSeat)
				.collect(HashSet::new, HashSet::add, HashSet::addAll);
		ArrayList<Long> free = new ArrayList<>();
		Long seats = event.getAuditoriums().get(dateTime).getNumberOfSeats();
		for (long i = 0; i < seats; i++) {
			if (!bookSeats.contains(i)) {
				free.add(i);
			}else{
				free.add(Long.valueOf(-1));
			}
		}
		return free;
	}

	@Nonnull
	@Override
	public Set<Ticket> getPurchasedTicketsForEvent(@Nonnull Event event, @Nonnull LocalDateTime dateTime) {
		return ticketDAO.getPurchasedTicketsForEvent(event, dateTime);
	}

	private Set<Long> ticketToSeat(Collection<Ticket> tickets) {
		return null;
	}
}
