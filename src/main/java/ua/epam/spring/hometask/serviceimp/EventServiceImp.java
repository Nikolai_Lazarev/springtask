package ua.epam.spring.hometask.serviceimp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.dao.daoimp.EventDAO;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.service.EventService;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class EventServiceImp implements EventService {

	@Autowired
	EventDAO eventDAO;

	public EventServiceImp(EventDAO eventDAO) {
		this.eventDAO = eventDAO;
	}

	@Nullable
	@Override
	public Event getByName(@Nonnull String name) {
		return eventDAO.getByName(name);
	}

	@Override
	public Event save(@Nonnull Event object) {
		return eventDAO.create(object);
	}

	@Override
	public void remove(@Nonnull Event object) {
		eventDAO.remove(object);
	}

	@Override
	public Event getById(@Nonnull Long id) {
		return eventDAO.getById(id);
	}

	@Nonnull
	@Override
	public Collection<Event> getAll() {
		List<Event> events = eventDAO.getAll();
		List<Event> results = new ArrayList<>();
		events.forEach(event -> {
			if(event.getAuditoriums() != null){
				results.add(event);
			}
		});
		return results;
	}
}
