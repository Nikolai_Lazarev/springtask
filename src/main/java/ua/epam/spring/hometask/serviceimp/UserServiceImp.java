package ua.epam.spring.hometask.serviceimp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.dao.daoimp.UserDAO;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.UserService;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;


@Component
public class UserServiceImp implements UserService {

	private UserDAO userDAO;

	@Autowired
	public UserServiceImp(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Nullable
	@Override
	public User getUserByEmailAndPassword(@Nonnull String email, String password) {
		return userDAO.getUserByEmailAndPassword(email, password);
	}

	@Override
	public User save(@Nonnull User object) {
		return userDAO.create(object);
	}

	@Override
	public void remove(@Nonnull User object) {
		userDAO.remove(object);
	}

	@Override
	public User getById(@Nonnull Long id) {
		return userDAO.getById(id);
	}

	@Nonnull
	@Override
	public Collection<User> getAll() {
		return userDAO.getAll();
	}
}
