package ua.epam.spring.hometask.manager;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class LocalizationManager extends Manager {
	private String resource;

	protected LocalizationManager(@Value("english.properties") String file) throws IOException {
		super(file);
		resource = file;
	}
}
