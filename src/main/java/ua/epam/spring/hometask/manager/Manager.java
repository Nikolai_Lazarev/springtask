package ua.epam.spring.hometask.manager;

import java.io.IOException;
import java.util.Properties;


public abstract class Manager {
    protected Properties properties;
    protected Manager instance;

    public Manager() {
    }

    protected Manager(String resourceFile) throws IOException {
        properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream(resourceFile));
    }

    public String getValue(String key) {
        return properties.getProperty(key);
    }
}
