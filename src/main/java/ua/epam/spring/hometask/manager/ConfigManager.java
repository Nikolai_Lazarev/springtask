package ua.epam.spring.hometask.manager;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component("configManager")
public class ConfigManager extends Manager {

    private String resource;

    protected ConfigManager(@Value("config.properties") String file) throws IOException {
        super(file);
        resource = file;
    }
}
